package com.joeyynchan.webcrawler.datastructure;

import org.junit.Assert;
import org.junit.Test;

import java.util.function.Function;

public class ResultTest {

    @Test
    public void okReturnOkMapperValue() {
        final Result<Integer, Integer> result = Result.ok(10);
        final Function<Integer, Integer> square = x -> x*x;
        final Function<Integer, Integer> cube = x -> x*x*x;
        final Integer expectedResult = 100;
        Assert.assertEquals(expectedResult, result.either(square, cube));
    }

    @Test
    public void errorReturnErrorMapperValue() {
        final Result<Integer, Integer> result = Result.error(3);
        final Function<Integer, Integer> square = x -> x*x;
        final Function<Integer, Integer> cube = x -> x*x*x;
        final Integer expectedResult = 27;
        Assert.assertEquals(expectedResult, result.either(square, cube));
    }

}
