package com.joeyynchan.webcrawler.web;

import org.junit.Assert;
import org.junit.Test;

public class URLTest {

    @Test
    public void telLink() {
        Assert.assertTrue(URL.of("tel:1234").isTelLink());
        Assert.assertTrue(URL.of("tel:29387238").isTelLink());
        Assert.assertTrue(URL.of("tel:472938268173").isTelLink());

        Assert.assertFalse(URL.of("mail:foo@bar.com").isTelLink());
        Assert.assertFalse(URL.of("www.google.com").isTelLink());
        Assert.assertFalse(URL.of("https://www.yahoo.com").isTelLink());
    }

    @Test
    public void mailLink() {
        Assert.assertTrue(URL.of("mail:helloworld@gmail.com").isMailLink());
        Assert.assertTrue(URL.of("mail:gmail@outlook.com").isMailLink());

        Assert.assertFalse(URL.of("tel:1928293").isMailLink());
    }


    @Test
    public void AnchorLink() {
        Assert.assertTrue(URL.of("#myname").isAnchorLink());
        Assert.assertTrue(URL.of("#here").isAnchorLink());

        Assert.assertFalse(URL.of("/absolutpath/hello/world").isAnchorLink());
    }

    @Test
    public void resolvePath() {
        Assert.assertEquals(
                URL.of("http://hello.com/apple/orange"),
                URL.of("http://hello.com/foo/bar").resolve(URL.of("/apple/orange")));

        Assert.assertEquals(
                URL.of("http://hello.com/foo/bar/apple/orange"),
                URL.of("http://hello.com/foo/bar/").resolve(URL.of("apple/orange")));

        Assert.assertEquals(
                URL.of("http://hello.com/foo/apple/orange"),
                URL.of("http://hello.com/foo/bar/").resolve(URL.of("../apple/orange")));
    }
}
