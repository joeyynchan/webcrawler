package com.joeyynchan.webcrawler.web;

import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

import static java.util.Collections.emptySet;

public class URLExtractorTest {

    private final URLExtractor urlExtractor = new URLExtractor();

    @Test
    public void urlInHyperlink() {
        final WebPageContent webPageContent = new WebPageContent("<a href=\"http://abc.com\">");
        Assert.assertEquals(Set.of(URL.of("http://abc.com")), urlExtractor.extract(webPageContent));

        final WebPageContent webPageContent2 = new WebPageContent("<link rel=\"manifest\" crossorigin=\"use-credentials\" href=\"http://abc.com\">");
        Assert.assertEquals(Set.of(URL.of("http://abc.com")), urlExtractor.extract(webPageContent2));

        final WebPageContent webPageContent3 = new WebPageContent("<a class=\"A8xHzb \" href=\"technologies?hl=en\">");
        Assert.assertEquals(Set.of(URL.of("technologies?hl=en")), urlExtractor.extract(webPageContent3));


    }

    @Test
    public void urlsInMultipleHyperlinks() {
        final WebPageContent webPageContent = new WebPageContent("<a href=\"http://abc.com\"><a href=\"http://apple.com\">");
        Assert.assertEquals(Set.of(URL.of("http://abc.com"), URL.of("http://apple.com")), urlExtractor.extract(webPageContent));
    }

    @Test
    public void urlInPlainTextIsIgnored() {
        final WebPageContent webPageContent = new WebPageContent("<div>http://abc.com</div>");
        Assert.assertEquals(emptySet(), urlExtractor.extract(webPageContent));
    }
}
