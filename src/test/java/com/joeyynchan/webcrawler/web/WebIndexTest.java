package com.joeyynchan.webcrawler.web;

import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

public class WebIndexTest {

    @Test
    public void sameFullURIIsValidChildUrl() {
        final WebIndex webIndex = new WebIndex(URL.of("http://helloworld.com/"), Set.of(URL.of("http://helloworld.com/")));
        Assert.assertEquals(Set.of(URL.of("http://helloworld.com/")), webIndex.getChildUrls());

        final WebIndex webIndex2 = new WebIndex(URL.of("http://helloworld.com/"), Set.of(URL.of("http://helloworld.com")));
        Assert.assertEquals(Set.of(URL.of("http://helloworld.com")), webIndex2.getChildUrls());
    }

    @Test
    public void fullUriOfSameHostIsValidChildUrl() {
        final WebIndex webIndex = new WebIndex(URL.of("http://helloworld.com/"), Set.of(URL.of("http://helloworld.com/foo/world")));
        Assert.assertEquals(Set.of(URL.of("http://helloworld.com/foo/world")), webIndex.getChildUrls());

        final WebIndex webIndex2 = new WebIndex(URL.of("http://helloworld.com/bar/world"), Set.of(URL.of("http://helloworld.com/foo")));
        Assert.assertEquals(Set.of(URL.of("http://helloworld.com/foo")), webIndex2.getChildUrls());
    }

    @Test
    public void absoluteUrlIsValidChildUrl() {

        final WebIndex webIndex = new WebIndex(URL.of("http://helloworld.com/"), Set.of(URL.of("/foo/world")));
        Assert.assertEquals(Set.of(URL.of("http://helloworld.com/foo/world")), webIndex.getChildUrls());
    }
}
