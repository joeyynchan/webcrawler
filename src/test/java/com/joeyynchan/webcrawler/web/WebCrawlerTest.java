package com.joeyynchan.webcrawler.web;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Sanity Test for debugging
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class WebCrawlerTest {

    @Autowired
    private WebCrawler webCrawler;

    @Test
    public void test() {
        webCrawler.crawl(URL.of("http://google.com"), 1);
    }

    @Test
    public void test2() {
        webCrawler.crawl(URL.of("http://google.com/intl/en/policies/privacy/"), 1);
    }

    @Test
    public void test3() {
        webCrawler.crawl(URL.of("http://bbc.co.uk"), 1);
    }

    @Test
    public void test4() {
        webCrawler.crawl(URL.of("http://9gag.com"), 1);
    }
}
