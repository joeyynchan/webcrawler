package com.joeyynchan.webcrawler.controller;

import com.joeyynchan.webcrawler.web.WebPage;

public class SitemapEntry {

    private final String url;
    private final String link;

    static SitemapEntry from(WebPage webPage) {
        final String url = webPage.getUrl().toUrlString();
        return new SitemapEntry(url, "/?url=" + url);
    }

    private SitemapEntry(String url, String link) {
        this.url = url;
        this.link = link;
    }

    public String getUrl() {
        return url;
    }

    public String getLink() {
        return link;
    }
}
