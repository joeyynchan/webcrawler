package com.joeyynchan.webcrawler.controller;

import com.joeyynchan.webcrawler.web.WebPage;

import java.util.Set;

import static java.util.stream.Collectors.toSet;

public class Sitemap {

    private final String url;
    private final Set<SitemapEntry> children;

    static Sitemap fromWebPage(WebPage webPage) {
        final String url = webPage.getUrl().toUrlString();
        final Set<SitemapEntry> children = webPage.getChildPages().stream().map(SitemapEntry::from).collect(toSet());
        return new Sitemap(url, children);
    }

    private Sitemap(String url, Set<SitemapEntry> children) {
        this.url = url;
        this.children = children;
    }

    public String getUrl() {
        return url;
    }

    public Set<SitemapEntry> getChildren() {
        return children;
    }

}
