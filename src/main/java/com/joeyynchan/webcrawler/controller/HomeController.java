package com.joeyynchan.webcrawler.controller;

import com.joeyynchan.webcrawler.datastructure.Error;
import com.joeyynchan.webcrawler.datastructure.Result;
import com.joeyynchan.webcrawler.web.URL;
import com.joeyynchan.webcrawler.web.WebCrawler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Application is decided inside-out.
 * Controller layer can be completely rewritten to support XML, UI etc.
 * Currently JSON is chosen because of simplicity. This is pretty much a REST API, following HATEOAS for navigation.
 * A JSON Restful API also allows frontend/backend separation, allowing frontend developers to develop UI in a different
 * language.
 */

@RestController
public class HomeController {

    @Autowired
    WebCrawler webCrawler;

    @GetMapping(produces="application/json")
    public ResponseEntity crawl(@RequestParam("url") String url) {
        return parseUrl(url)
                .map(url1 -> webCrawler.crawl(url1, 1))
                .mapOk(Sitemap::fromWebPage)
                .either(
                        sitemap -> ResponseEntity.ok().body(sitemap),
                        error -> ResponseEntity.badRequest().body(error));
    }

    private Result<URL, Error> parseUrl(String url) {
        try {
            return Result.ok(URL.of(url));
        } catch (IllegalArgumentException exception) {
            return Result.error(Error.of("Invalid URL - " + url));
        }
    }

}
