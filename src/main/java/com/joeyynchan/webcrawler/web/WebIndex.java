package com.joeyynchan.webcrawler.web;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Object which represents the concept of combination of a url and its child urls. WebIndex contains logic to decide
 * what urls are deemed valid child urls.
 */
public class WebIndex {

    private final URL url;
    private final Set<URL> childUri;

    public WebIndex(URL url, Set<URL> childUri) {
        this.url = url;
        this.childUri = childUri;
    }

    public Set<URL> getChildUrls() {
        return childUri.stream()
                .map(url -> url.isRelative() ? this.url.resolve(url) : url)
                .filter(url -> !url.isAnchorLink())
                .filter(url -> !url.isMailLink())
                .filter(url -> !url.isTelLink())
                .filter(url -> url.subdomainOf(this.url))
                .collect(Collectors.toSet());
    }
}
