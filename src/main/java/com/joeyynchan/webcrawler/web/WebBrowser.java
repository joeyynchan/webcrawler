package com.joeyynchan.webcrawler.web;

import com.joeyynchan.webcrawler.datastructure.Error;
import com.joeyynchan.webcrawler.datastructure.Result;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * A decorator for httpClient gives us flexbility to replace the client implementation with a different one.
 * I opted for OkHttpClient over RestTemplate because of the better readability of the code since most of the objects
 * in okHttpClient can be configured using Builder pattern.
 *
 * To avoid throwing checked exception outside of the class (which some developers use to manipulate workflow). The monad-style
 * Result class is used.
 */

@Component
public class WebBrowser {

    private final OkHttpClient okHttpClient;

    @Autowired
    public WebBrowser(OkHttpClient okHttpClient) {
        this.okHttpClient = okHttpClient;
    }

    public Result<WebPageContent, Error> browse(URL url) {
        try {
            final Request request = new Request.Builder().url(url.toUrlString()).get().build();
            final Response response = okHttpClient.newCall(request).execute();
            return Result.ok(new WebPageContent(response.body().string()));
        } catch (RuntimeException | IOException e) {
            return Result.error(Error.of(e.getMessage()));
        }
    }

}
