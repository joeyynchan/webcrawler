package com.joeyynchan.webcrawler.web;

import com.joeyynchan.webcrawler.datastructure.Error;
import com.joeyynchan.webcrawler.datastructure.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

/**
 * Essentially a cache for web index to avoid querying the same data over and over again.
 * This can be considered as the repository layer of the application and be replaced with a database.
 *
 * Library always attempts fo find information from its 'cache'. If it fails to do so, Library will compute the value and store
 * it for future use.
 */

@Component
public class WebIndexLibrary {

    private final WebBrowser webBrowser;
    private final URLExtractor urlExtractor;
    private final Map<URL, Result<WebIndex, Error>> webIndexCache;

    @Autowired
    public WebIndexLibrary(WebBrowser webBrowser, URLExtractor urlExtractor) {
        this.webBrowser = webBrowser;
        this.urlExtractor = urlExtractor;
        this.webIndexCache = new ConcurrentHashMap<>();
    }

    public Result<WebIndex, Error> getWebIndex(URL url) {
        webIndexCache.computeIfAbsent(url, fetchWebIndex());
        return webIndexCache.get(url);
    }

    private Function<URL, Result<WebIndex, Error>> fetchWebIndex() {
        return url -> webBrowser.browse(url)
                .map(webPageContent -> Result.ok(urlExtractor.extract(webPageContent)))
                .mapOk(urls -> new WebIndex(url, urls));
    }

}
