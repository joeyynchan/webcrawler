package com.joeyynchan.webcrawler.web;

import com.joeyynchan.webcrawler.datastructure.Error;
import com.joeyynchan.webcrawler.datastructure.Result;
import org.springframework.stereotype.Component;

/**
 * The central component of the whole application. This is the highest level class in the business-logic layer.
 * Controllers communicate with this class to extract most of the information.
 *
 * Web indices are extract from WebIndexLibrary and are used to build the WebPage object to be returned to the controller.
 */

@Component
public class WebCrawler {

    private final WebIndexLibrary webIndexLibrary;

    public WebCrawler(WebIndexLibrary webIndexLibrary) {
        this.webIndexLibrary = webIndexLibrary;
    }

    public Result<WebPage, Error> crawl(URL url, int depthToLive) {

        if (depthToLive <= 0) {
            return Result.ok(new WebPage(url));
        } else {
            // Possible improvement: Improve Error handling instead of returning an empty WebIndex
            return webIndexLibrary.getWebIndex(url)
                    .mapOk(webIndex -> formWebPage(url, webIndex, depthToLive));
        }

    }

    private WebPage formWebPage(URL url, WebIndex webIndex, int depthToLive) {
        final WebPage webPage = new WebPage(url);

        // Recursive approach should we need to crawl a webpage to a specific depth
        // Possible improvement: Replace iteration with tail recursion to avoid stack overflow
        for (URL childUrl : webIndex.getChildUrls()) {
            crawl(childUrl, depthToLive-1).executeIfOk(webPage::addChildPage);
        }
        return webPage;
    }

}
