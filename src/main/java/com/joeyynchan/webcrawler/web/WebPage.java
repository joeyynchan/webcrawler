package com.joeyynchan.webcrawler.web;

import java.util.*;

public class WebPage {

    private final URL url;
    private final Set<WebPage> childPages;

    public WebPage(URL url) {
        this.url = url;
        this.childPages = new HashSet<>();
    }

    public WebPage(URL url, Set<WebPage> childPages) {
        this.url = url;
        this.childPages = childPages;
    }

    public void addChildPage(WebPage webPage) {
        this.childPages.add(webPage);
    }

    public URL getUrl() {
        return url;
    }

    public Set<WebPage> getChildPages() {
        return childPages;
    }
}
