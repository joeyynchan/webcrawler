package com.joeyynchan.webcrawler.web;

import java.net.URI;
import java.util.Objects;

/**
 * Wrapper for java.net.URI with additional methods for higher readability and class is OO-like
 * Having a URL Wrapper allows us to easily switch if we decide there is a better alternative for java.net.URI
 */
public class URL {

    private final java.net.URI value;

    public static URL of(String url) {
        return new URL(url);
    }

    private URL(String value) {
        this.value = URI.create(value.trim());
    }

    public final boolean isRelative() {
        return value.getScheme() == null && value.getHost() == null;
    }

    public final URL resolve(URL url) {
        return URL.of(value.resolve(url.value).toString());
    }

    public final boolean isTelLink() {
        return "tel".equalsIgnoreCase(value.getScheme());
    }

    public final boolean isMailLink() {
        return "mail".equalsIgnoreCase(value.getScheme());
    }

    public final boolean isAnchorLink() {
        return value.toString().startsWith("#") && !value.toString().contains("/");
    }

    public final boolean subdomainOf(URL otherUrl) {
        return value.getHost() == null || value.getHost().endsWith(otherUrl.value.getHost());
    }

    @Override
    public String toString() {
        return "URL{" +
                "value=" + value +
                '}';
    }

    public String toUrlString() {
        return value.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        URL url = (URL) o;
        return Objects.equals(value, url.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
