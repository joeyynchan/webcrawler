package com.joeyynchan.webcrawler.web;

/**
 * Simple class to represent HTML rather than using String directly to avoid primitive obsession.
 */
public class WebPageContent {

    private final String html;

    public WebPageContent(String html) {
        this.html = html;
    }

    public String getHtml() {
        return html;
    }
}
