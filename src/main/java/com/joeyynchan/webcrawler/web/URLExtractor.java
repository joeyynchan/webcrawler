package com.joeyynchan.webcrawler.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
/**
 * A URLExtractor is used instead of embedding url extraction logic inside WebPageContent class. If we need to change
 * the criteria with what URL we deem valid, we can easily substitute the logic with the help of DI.
 */
public class URLExtractor {

    private Logger logger = LoggerFactory.getLogger(URLExtractor.class);

    private static final Pattern HREF_PATTERN = Pattern.compile("<[^>]*?href=\"([^\"]*)\"[^>]*?>");

    public Set<URL> extract(WebPageContent webPageContent) {
        final Matcher matcher = HREF_PATTERN.matcher(webPageContent.getHtml());
        final Set<URL> matches = new HashSet<>();
        while (matcher.find()) {
            final String url = matcher.group(1);
            try {
                matches.add(URL.of(url));
            } catch (IllegalArgumentException e) {
                logger.error("{" + url + "} cannot be parse as URL");
            }
        }
        return matches;
    }

}
