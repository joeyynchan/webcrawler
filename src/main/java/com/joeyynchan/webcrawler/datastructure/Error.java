package com.joeyynchan.webcrawler.datastructure;

/**
 * This is a helper class to help avoiding checked exception handling in java. See Result class for further information.
 */
public class Error {

    private final String value;

    public static Error of(String value) {
        return new Error(value);
    }

    private Error(String value) {
        this.value = value;
    }

    public String getError() {
        return value;
    }
}
