package com.joeyynchan.webcrawler.datastructure;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Here we are immitating monad in Functional programming to avoid using try-catch for checked exception handling.
 * It is similar to Optional but it also provide a second field to keep track of the errors when something goes wrong.
 * @param <E> For now, it is always Error
 */
public class Result<V, E> {

    private final E error;
    private final V value;

    public static <V, E> Result<V, E> ok(V value) {
        return new Result<>(Objects.requireNonNull(value), null);
    }

    public static <V, E> Result<V, E> error(E error) {
        return new Result<>(null, Objects.requireNonNull(error));
    }

    private Result(V value, E error) {
        this.value = value;
        this.error = error;
    }

    public <T> T either(Function<V, T> okFunction, Function<E, T> errorFunction) {
        return error == null
                ? okFunction.apply(value)
                : errorFunction.apply(error);
    }

    public <T> Result<T, E> map(Function<V, Result<T, E>> function) {
        return error == null
                ? function.apply(value)
                : error(error);
    }

    public <T> Result<T, E> mapOk(Function<V, T> function) {
        return error == null
                ? Result.ok(function.apply(value))
                : error(error);
    }

    public Result<V, E> executeIfOk(Consumer<V> consumer) {
        if (error == null)
            consumer.accept(value);
        return this;
    }

    public V orElse(V defaultValue) {
        return error == null ? value : defaultValue;
    }

}
