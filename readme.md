# Simple Web Crawler

## Run Application

### Option 1: Run With Maven (**Require Java 11**)
```mvn spring-boot:run``` 

### Option 2: Run JAR
```java -jar webcrawler-0.1-SNAPSHOT.jar```

Navigate to `http://localhost:8080/?url=<INSERT_URL_TO_CRAWL_HERE>`. Replace `<INSERT_URL_TO_CRAWL_HERE>` with the url you wish to crawl


## Notes
There's comments throughout different classes to explain motive behind each class. Below are also some explanation to some high level decisions I have made

#### Why did I use `@Component` instead of `@Service` and `@Repository`
The MVC architecture promotes separation of responsibility, at the same time to me it also discourages OO-programming at the same time since code in service layers (in my experience) are always very procedural and all the function related to the same domain will be chucked into the same `@Service`. I prefer having `@Component` to constant reminds me I should try to design my code as OO-like as possible (For example `dog.speak()` over `speakerService.speak(dog)`.

#### If I prefer OO-Programming, what's with all the data objects and streams?
I think OO-programming in high level is more easier to understand. It is relatively intuitive to understand the relationship amongs WebCrawler, WebBrowser, and WebLibrary. It makes sense if a crawler needs to crawl a website, it probably needs a browser to perform the web browsing part. However when it comes to data processing, I think this sort of procedural programming is more readable and easier to be modified. I think a combination of both oo-programming and procedural programming are essential for modern applications. Findind the right balance is always something I want toa achieve.

#### Why didn't I use any VCS?
Most of the time when I have to design a new application which I am unfamiliar with the concept, I always start with a prototype and since I change my code so often to explore different balances, I usually only commit once a sizable chuck is finished. This is considered a prototype to me. (Although I did spend a considerate amount of time polishing the code to make it look nicer)
